	// WebSocket
	var socket = io.connect();
	// initial slider settings
	var sliders = [];
	var oldSliderID = "";
	var oldValue = 0;

	var switches = [];
	

	function sendSlider(SliderID, Value) {
		if ( SliderID == this.oldSliderID && Value == this.oldValue ) {
			console.log("send slider duplicate: " + SliderID + " = " + Value);
		} else {
			socket.emit('slider', { name: SliderID, value: Value });
			console.log("send slider: " + SliderID + " = " + Value);
			this.oldSliderID = SliderID;
			this.oldValue = Value;
		}
	}

	function sendSwitch(SwitchID, Value) {
		socket.emit('switch', { name: SwitchID, value: Value });
		console.log("send switch: " + SwitchID + " = " + Value);
	}

	function sendValue(VariableID, Value) {
		socket.emit('setvalue', { name: VariableID, value: Value });
		//console.log("send value: " + VariableID + " = " + Value);
	}

	function requestSettings(Settings) {
		socket.emit('settings', { name: Settings });
		console.log("request settings: " + Settings);
	}

	function setSlider(SliderID, Value) {
		var E1 = document.getElementById(SliderID + "Out");
		var E2 = document.getElementById(SliderID + "In");
		var E3 = document.getElementById(SliderID);
		console.log("actually setSlider: " + SliderID + " = " + Value);
		if (E1) {
                    E1.innerHTML = Value;
		};
		if (E2) {
                    E2.value = Value;
		};
		if (E3) {
                    E3.value = Value;
		};
		//console.log("setSlider Action: " + sliders[SliderID].Action);
		//eval ( window.sliders[SliderID].Action );
	}
	
	// perform switch settings	
	function setSwitch(SwitchID, Value) {
		console.log("setSwitch SwitchID: " + SwitchID + " Action: " + window.switches[SwitchID].Action + " Value = " + Value);
		var T1 = document.getElementById(SwitchID+"sine");
		if (T1) {
			T1.style.borderColor="white";
		};
		var T2 = document.getElementById(SwitchID+"square");
		if (T2) {
			T2.style.borderColor="white";
		};
		var T3 = document.getElementById(SwitchID+"sawtooth");
		if (T3) {
			T3.style.borderColor="white";
		};
		var T4 = document.getElementById(SwitchID+"triangle");
		if (T4) {
			T4.style.borderColor="white";
		};
		var T5 = document.getElementById(SwitchID+"maxvol");
		if (T5) {
			T5.style.borderColor="white";
		};
		var T6 = document.getElementById(SwitchID+"true");
		if (T6) {
			T6.style.borderColor="white";
		};
		var T7 = document.getElementById(SwitchID+"false");
		if (T7) {
			T7.style.borderColor="white";
		};
		var T8 = document.getElementById(SwitchID+"volume");
		if (T8) {
			T8.style.borderColor="white";
		};
		var T9 = document.getElementById(SwitchID+"balance");
		if (T9) {
			T9.style.borderColor="white";
		};
		var T0 = document.getElementById(SwitchID+"2ch");
		if (T0) {
			T0.style.borderColor="white";
		};
		var Ta = document.getElementById(SwitchID+Value);
		if (Ta) {
			Ta.style.borderColor="red";
		};

		//eval ( switches[SwitchID].Action );
	}
	
	function printValue(sliderID, textbox) {
		var x = document.getElementById(textbox);
		var y = document.getElementById(sliderID);
		x.value = y.value;
	}

	//
	function SelWaveForm(img,v,WaveArt) {
		/*
		document.getElementById("sine"+v).style.borderColor="white";
		document.getElementById("square"+v).style.borderColor="white";
		document.getElementById("sawtooth"+v).style.borderColor="white";
		var T1 = document.getElementById("triangle"+v);
		if (T1) {
			T1.style.borderColor="white";
		};
		var T2 = document.getElementById("maxvol"+v);
		if (T2) {
			T2.style.borderColor="white";
		};
		img.style.borderColor="red"; 
		*/
		sendSwitch("sl"+v+"Type", WaveArt);
	}

	function SelMicTarget(img,v,WaveArt)
	{
		/*
		document.getElementById("volume"+v).style.borderColor="white";
		document.getElementById("balance"+v).style.borderColor="white";
		document.getElementById("2ch"+v).style.borderColor="white";
		document.getElementById("maxvol"+v).style.borderColor="white";
		img.style.borderColor="red"; 
		*/
		sendSwitch("sl"+v+"Type", WaveArt);
	}
	
	function MasterMaxVol(channel) {
		event.preventDefault();
	console.log("MasterMaxVol: " + channel);
	if ( channel == "left" ) {
		//this.MasterVolLeft = this.gainNodeLeft.gain.value;
		//this.gainNodeLeft.gain.value = 1;
		document.getElementById("maxVolLeft").style.borderColor = "red";
		// document.getElementById("maxVolLeft").src = "./speaker_green.png";
		sendSwitch("MasterMaxVol", "left");
	} else if ( channel == "right" ) {
		//this.MasterVolRight = this.gainNodeRight.gain.value;
		//this.gainNodeRight.gain.value = 1;
		document.getElementById("maxVolRight").style.borderColor = "red";
		sendSwitch("MasterMaxVol", "right");
		};
	};

	function MasterRestoreVol(channel) {
		if ( channel == "left" ) {
			//this.gainNodeLeft.gain.value = this.MasterVolLeft;
			//this.MasterVolLeft = 0.01;
			document.getElementById("maxVolLeft").style.borderColor = "white";
			// document.getElementById("maxVolLeft").src = "./speaker_green.png";
			sendSwitch("MasterRestoreVol", "left");
		} else if ( channel == "right" ) {
			//this.gainNodeRight.gain.value = this.MasterVolRight;
			//this.MasterVolRight = 0.01;
			document.getElementById("maxVolRight").style.borderColor = "white";
			sendSwitch("MasterRestoreVol", "right");
			};
	};

$(document).ready(function()
{
	//socket.on('settings', function ( { sliders: sliders, switches: switches } ) {
	socket.on('settings', function ( data ) {
		console.log("socket.on settings");
		// receive a complete set of settings
		window.sliders = data.sliders;
		window.switches = data.switches;
        this.SliderID = "";
		this.SwitchID = "";
		for (SliderID in window.sliders) {
			console.log('settings -> SliderID: ' + SliderID + " " + window.sliders[SliderID].Val);
			var E1 = document.getElementById(SliderID + "Label");
			if (E1) {
                    E1.innerHTML = window.sliders[SliderID].Label;
			};
			var E2 = document.getElementById(SliderID);
			if (E2) {
				if (E2.hasOwnProperty('min' ))
				{
						E2.min = window.sliders[SliderID].Min;
				}
				if (E2.hasOwnProperty('max'))
				{
						E2.min = window.sliders[SliderID].Max;
				}
				if (E2.hasOwnProperty('step'))
				{
						E2.min = window.sliders[SliderID].Step;
				}
			}
			var E2 = document.getElementById(SliderID + "In");
			if (E2) {
				if (E2.hasOwnProperty('min'))
				{
						E2.min = window.sliders[SliderID].Min;
				}
				if (E2.hasOwnProperty('max'))
				{
						E2.min = window.sliders[SliderID].Max;
				}
				if (E2.hasOwnProperty('step'))
				{
						E2.min = window.sliders[SliderID].Step;
				}
			}
			/* console.log('settings -> SliderID: ' + SliderID + " "
			+ window.sliders[SliderID].Label + " "
			+ window.sliders[SliderID].Min + " "
			+ window.sliders[SliderID].Max + " "
			+ window.sliders[SliderID].Step + " "
			+ window.sliders[SliderID].Action);
			 */
			 setSlider(SliderID, window.sliders[SliderID].Val);
		}
		for (SwitchID in window.switches) {
			/* console.log('settings -> SwitchID: ' + SwitchID + " "
			+ window.switches[SwitchID].Val + " "
			+ window.switches[SwitchID].Action);
			*/
			setSwitch(SwitchID, window.switches[SwitchID].Val);
		}

	});

	socket.on('slider', function (data) {
		// receive and set slider value
		console.log("client received: " + data.name + " = " + data.value);
		setSlider(data.name, data.value);
	});

	socket.on('switch', function (data) {
		// receive switch value and set it
		console.log("client received switch: " + data.name + " = " + data.value);
		setSwitch(data.name, data.value);
	});

	socket.on('setvalue', function (data) {
		// set variable value
		//console.log("client received setvalue: " + data.name + " = " + data.value);
		eval ("sample." + data.name + " = " + data.value + ";");
	});


	// new message
	socket.on('chat', function (data) {
		var mtime = new Date(data.mtime);
		$('#content').append(
			$('<li></li>').append(
				// time
				$('<span>').text('[' +
					(mtime.getHours() < 10 ? '0' + mtime.getHours() : mtime.getHours())
					+ ':' +
					(mtime.getMinutes() < 10 ? '0' + mtime.getMinutes() : mtime.getMinutes())
					+ '] '
				),
				// Name
				$('<b>').text(typeof(data.name) != 'undefined' ? data.name + ': ' : ''),
				// Text
				$('<span>').text(data.text))
		);
		// scroll down
		$('body').scrollTop($('body')[0].scrollHeight);
	});

	
	// send message
	function send(){
		// read input
		var name = $('#name').val();
		var text = $('#text').val();
		// send to socket
		socket.emit('chat', { name: name, text: text });
		// empty input area
		$('#text').val('');
	}
	
	
	// on click
	$('#send').click(send);
	// on key press "enter"
	$('#text').keypress(function (e) {
		if (e.which == 13) {
			send();
		}
	});

	$(console.log("Starting client3.js stuff"));
	$(requestSettings("default"));
	$(console.log("settings requested"));
	//$(setSwitch("sl31Type","2ch"));

});	

