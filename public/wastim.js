/*
 * part of the webaudio is based upon work made in 2013 by Boris Smus.
 * 
 * heavily modified and turned into an eStim generator:
 * BackLoop since 2015 
 */


function WAStim(stream,client) {
	for (i in stream) {console.log("i = " + i)};
	console.log ("getAudiotracks = " + stream.getAudiotracks);
	this.stream = stream;
	this.client = client;
	//console.log( "stream init = " +  stream );
	this.isPlayingOsc1 = false;
	this.isPlayingOsc2 = false;
	
	// maybe this needs to be rethought: does this disable the other client's output?
	this.isPlayingMaster1 = false;
	this.isPlayingMaster2 = false;
	
	this.isPlaying = false;
	this.MicMax = false;
	this.MicMasterVol = false;
	// mic influences master volume
	this.MicBalance = false;
	// mic influences balance
	this.Mic2ch = false;
	// each mic channel influences its sides volume
	this.oldMicVol = 1;
	//MicVol1 & 2 store the maximum mic value per frame per client
	this.MicVol1 = 0.1;
	this.MicVol2 = 0.2;
	// define which MicVol(1/2) to set, depending on client ID
	this.MicVolID = "MicVol" + this.client;
	// remember old gain values for MaxVol
	this.MasterVolLeft = 0.01;
	this.MasterVolRight = 0.01;
	this.canvas1 = document.querySelector('#Osc1canvas');
	this.canvas2 = document.querySelector('#Osc2canvas');
	this.canvasMic = document.querySelector('#Miccanvas');
	this.WIDTH = 200;
	this.HEIGHT = 120;
}

	
WAStim.prototype.play = function() {
	// Create nodes.
	this.merger = context.createChannelMerger(2);
	this.gainNodeMaster = context.createGain();
	this.muteMaster1 = context.createGain();		// final mute for client 1
	this.muteMaster2 = context.createGain();		// final mute for client 2
	if (this.client == 1) {
		this.muteMasterClient = this.muteMaster1;
	} else if (this.client == 2) {
		this.muteMasterClient = this.muteMaster2;
	}
	this.gainNodeLeft = context.createGain();
	this.gainNodeRight = context.createGain();
	this.gainNodeMic = context.createGain();
	this.osc1 = context.createOscillator();
	this.lfo1 = context.createOscillator();
	this.gainNodeLfo1 = context.createGain();
	this.analyser1 = context.createAnalyser();
	this.gainNode1 = context.createGain();
	this.muteNode1 = context.createGain();

	// Setup the graph.
	//this.osc1.connect(this.gainNode1);
	this.lfo1.connect(this.gainNodeLfo1.gain);
	this.osc1.connect(this.gainNodeLfo1);
	this.gainNodeLfo1.connect(this.gainNode1);
	this.gainNode1.connect(this.muteNode1);
	this.gainNode1.gain.value = 0.01;
	this.muteNode1.connect(this.gainNodeLeft);
	this.gainNodeLeft.connect(this.merger, 0, 0 );
	this.gainNodeLeft.connect(this.analyser1);

	// oscillator set 2
	this.osc2 = context.createOscillator();
	this.lfo2 = context.createOscillator();
	this.gainNodeLfo2 = context.createGain();
	this.analyser2 = context.createAnalyser();
	this.gainNode2 = context.createGain();
	this.muteNode2 = context.createGain();

	// Setup the graph.
	//this.osc1.connect(this.gainNode2);
	this.lfo2.connect(this.gainNodeLfo2.gain);
	this.osc2.connect(this.gainNodeLfo2);
	this.gainNodeLfo2.connect(this.gainNode2);
	this.gainNode2.connect(this.muteNode2);
	this.gainNode2.gain.value = 0.01;
	this.muteNode2.connect(this.gainNodeRight);
	this.gainNodeRight.connect(this.merger, 0, 1);
	this.gainNodeRight.connect(this.analyser2);

	// initialize mic source
	// console.log("this.stream = " + this.stream.id);
	sourceMic = context.createMediaStreamSource(this.stream);

	// initialize audio element source
	//sourceAudio = context.createMediaElementSource(myAudio);
	this.analyserMic = context.createAnalyser();
	sourceMic.connect(this.gainNodeMic);
	this.gainNodeMic.connect(this.analyserMic);
	// this.gainNodeMic.connect(this.gainNodeMaster); //should this connection exist?
	// i think the connection above would only route the mic input to be mixed with the final output
	this.gainNodeMic.gain.value = 0.01;

	// mixer and output
	this.merger.connect(this.gainNodeMaster);
	this.gainNodeMaster.connect(this.muteMasterClient);
	this.muteMasterClient.connect(context.destination);

	this.osc1[this.osc1.start ? 'start' : 'noteOn'](0);
	this.lfo1[this.lfo1.start ? 'start' : 'noteOn'](0);
	this.osc2[this.osc2.start ? 'start' : 'noteOn'](0);
	this.lfo2[this.lfo2.start ? 'start' : 'noteOn'](0);

	requestAnimFrame(this.visualize1.bind(this));
	requestAnimFrame(this.visualize2.bind(this));
	requestAnimFrame(this.visualizeMic.bind(this));
};

WAStim.prototype.stop = function() {
  this.osc1.stop(0);
  this.lfo1.stop(0);
  this.osc2.stop(0);
  this.lfo2.stop(0);
};

WAStim.prototype.toggle = function() {
  (this.isPlaying ? this.stop() : this.play());
  this.isPlaying = !this.isPlaying;

};

WAStim.prototype.EnableOsc1 = function(e) {
	if (e === "true") {
		this.muteNode1.gain.value = 1;
	} else {
		this.muteNode1.gain.value = 0.001;
	}
    this.isPlayingOsc1 = e;
};

WAStim.prototype.changeFrequencyOsc1 = function(val) {
  this.osc1.frequency.value = val;
};

WAStim.prototype.changeDetuneOsc1 = function(val) {
  this.osc1.detune.value = val;
};

WAStim.prototype.changeTypeOsc1 = function(type) {
  this.osc1.type = type;
};

WAStim.prototype.changeFrequencyLfo1 = function(val) {
  this.lfo1.frequency.value = val;
};

WAStim.prototype.changeDetuneLfo1 = function(val) {
  this.lfo1.detune.value = val;
};

WAStim.prototype.changeTypeLfo1 = function(type) {
 
  if ( type == "maxvol" ) {
	 this.osc1.connect(this.gainNode1); //connect osc1 output directly to gainNode1
	 this.lfo1.disconnect(); //disable lfo1
  } else {
	 this.osc1.connect(this.gainNodeLfo1); //connect osc1 output to gainNodeLfo1
	 this.lfo1.connect(this.gainNodeLfo1.gain); //enable lfo1
	 this.lfo1.type = type;
  };
};

WAStim.prototype.visualize1 = function() {
  this.canvas1.width = this.WIDTH;
  this.canvas1.height = this.HEIGHT;
  var drawContext = this.canvas1.getContext('2d');

  var times = new Uint8Array(this.analyser1.frequencyBinCount);
  this.analyser1.getByteTimeDomainData(times);
  for (var i = 0; i < times.length; i++) {
    var value = times[i];
    var percent = value / 256;
    var height = this.HEIGHT * percent;
    var offset = this.HEIGHT - height - 1;
    var barWidth = this.WIDTH/times.length;
    drawContext.fillStyle = 'black';
    drawContext.fillRect(i * barWidth, offset, 1, 1);
  }
  requestAnimFrame(this.visualize1.bind(this));
};

WAStim.prototype.changeVolume1 = function(element) {
  var volume = element.value;
  var fraction = parseInt(element.value) / parseInt(element.max);
  console.log( fraction + " " +  parseInt(element.value) + " " +  parseInt(element.max));
  // Let's use an x*x curve (x-squared) since simple linear (x) does not
  // sound as good.
  this.gainNode1.gain.value = fraction * fraction;
};

WAStim.prototype.EnableOsc2 = function(e) {
	if (e === "true") {
		this.muteNode2.gain.value = 1;
	} else {
		this.muteNode2.gain.value = 0.001;
	}
    this.isPlaying = e;
};

WAStim.prototype.changeFrequencyOsc2 = function(val) {
  this.osc2.frequency.value = val;
};

WAStim.prototype.changeDetuneOsc2 = function(val) {
  this.osc2.detune.value = val;
};

WAStim.prototype.changeTypeOsc2 = function(type) {
  this.osc2.type = type;
};

WAStim.prototype.changeFrequencyLfo2 = function(val) {
  this.lfo2.frequency.value = val;
};

WAStim.prototype.changeDetuneLfo2 = function(val) {
  this.lfo2.detune.value = val;
};

WAStim.prototype.changeTypeLfo2 = function(type) {
  if ( type == "maxvol" ) {
	this.osc2.connect(this.gainNode2); //connect osc2 output directly to gainNode2
	  this.lfo2.disconnect(); //disable lfo2
  } else {
	  this.osc2.connect(this.gainNodeLfo2); //connect osc2 output to gainNodeLfo2
	  this.lfo2.connect(this.gainNodeLfo2.gain); //enable lfo2
	  this.lfo2.type = type;
  };};

WAStim.prototype.visualize2 = function() {
  this.canvas2.width = this.WIDTH;
  this.canvas2.height = this.HEIGHT;
  var drawContext = this.canvas2.getContext('2d');

  var times = new Uint8Array(this.analyser2.frequencyBinCount);
  this.analyser2.getByteTimeDomainData(times);
  for (var i = 0; i < times.length; i++) {
    var value = times[i];
    var percent = value / 256;
    var height = this.HEIGHT * percent;
    var offset = this.HEIGHT - height - 1;
    var barWidth = this.WIDTH/times.length;
    drawContext.fillStyle = 'black';
    drawContext.fillRect(i * barWidth, offset, 1, 1);
  }
  requestAnimFrame(this.visualize2.bind(this));
};

WAStim.prototype.changeVolume2 = function(element) {
  var volume = element.value;
  var fraction = parseInt(element.value) / parseInt(element.max);
  console.log( fraction + " " +  parseInt(element.value) + " " +  parseInt(element.max));
  // Let's use an x*x curve (x-squared) since simple linear (x) does not
  // sound as good.
  this.gainNode2.gain.value = fraction * fraction;
};

WAStim.prototype.visualizeMic = function() {
	this.canvasMic.width = 40;
	this.canvasMic.height = 100;
	var drawContext = this.canvasMic.getContext('2d');
	// create a gradient for the fill. Note the strange
	// offset, since the gradient is calculated based on
	// the canvas, not the specific element we draw
	var gradient = drawContext.createLinearGradient(0,0,0,130);
	gradient.addColorStop(1,'#000000');
	gradient.addColorStop(0.75,'#ff0000');
	gradient.addColorStop(0.25,'#ffff00');
	gradient.addColorStop(0,'#ffffff');
	var max = 0;
	var values = 0;
	var average;
	var max = 0;
	var micSlider = document.getElementById("sl31");
	var micFraction = parseInt(micSlider.value) / parseInt(micSlider.max);
	var micVol = 0.001;
	var masterSlider = document.getElementById("sl33");
	var masterFraction = parseInt(masterSlider.value) / parseInt(masterSlider.max);
	var times = new Uint8Array(this.analyserMic.frequencyBinCount);
	this.analyserMic.getByteFrequencyData(times);

	for (var i = 0; i < times.length; i++) {
		var value = times[i];
		values += value;
		if (value > max) {max = value};
//    var percent = value / 256;
//    var height = this.HEIGHT * percent;
//    var offset = this.HEIGHT - height - 1;
//    var barWidth = this.WIDTH/times.length;
//    drawContext.fillStyle = 'black';
//    drawContext.fillRect(i * barWidth, offset, 1, 1);
	}
	average = values / times.length;
	max = max / 256 ;

	// clear the current state
	drawContext.clearRect(0, 0, this.canvasMic.width, this.canvasMic.height);
	// set the fill style
	drawContext.fillStyle=gradient;
	// create the meters
//	drawContext.fillRect(0,this.canvasMic.height-average,this.canvasMic.width/2-5,this.canvasMic.height);
	drawContext.fillRect(0,this.canvasMic.height - this.MicVol1 * this.canvasMic.height,this.canvasMic.width/2-5,this.canvasMic.height);
	//drawContext.fillRect(this.canvasMic.width/2,this.canvasMic.height - maxL * this.canvasMic.height,this.canvasMic.width/2-5,this.canvasMic.height);
	drawContext.fillRect(this.canvasMic.width/2,this.canvasMic.height - this.MicVol2 * this.canvasMic.height,this.canvasMic.width/2-5,this.canvasMic.height);
	micVol = max * micFraction;
	// only send updated values if there has been a certain change
	// console.log( this.oldMicVol +" " + micVol +" "+ max +" "+ micSlider.value);
	//console.log( micVolL +" " + micVolR +" "+ this.Mic2ch);
	//	console.log( this.Mic2ch);
	if ( Math.abs(this.oldMicVol - micVol) > 0.01 ) {
		this.oldMicVol = micVol;
		//sendSlider("sl33", micVol);
		//console.log( "this.MicVol1 = " + this.MicVol1);
	
		
		if ( this.MicMasterVol ) {
			//influence master volume
			this.MicVol1 = micVol;
			this.MicVol2 = micVol;
			this.gainNodeMaster.gain.value = ( masterFraction + micVol ) ;
		} else if ( this.MicBalance ) {
			this.MicVol1 = micVol;
			this.MicVol2 = micVol;
			//influence balance
			this.gainNodeRight.gain.value = (1 - micVol);
			this.gainNodeLeft.gain.value = micVol;
		} else if ( this.Mic2ch ) {
			//influence channels individually
			sendValue(this.MicVolID, micVol);
			console.log( "this.MicVol1 = " + this.MicVol1);
			this.gainNodeLeft.gain.value = this.MicVol1;
			this.gainNodeRight.gain.value = this.MicVol2;
		}
	}
	requestAnimFrame(this.visualizeMic.bind(this));
};

WAStim.prototype.changeVolumeMic = function(element) {
  var volume = element.value;
  var fraction = parseInt(element.value) / parseInt(element.max);
  console.log( fraction + " " +  parseInt(element.value) + " " +  parseInt(element.max));
  // Let's use an x*x curve (x-squared) since simple linear (x) does not
  // sound as good.
  this.gainNodeMic.gain.value = fraction * fraction;
  if ( this.MicMax ) {
	this.gainNodeMic.gain.value = 1.0; // eventually add some time ramp here
  };
		
};

WAStim.prototype.changeTypeMic = function(type) {

  if ( type == "volume" ) {
	this.MicMasterVol = true;
  } else {
	 this.MicMasterVol = false;
  };
  if ( type == "balance" ) {
	this.MicBalance = true;
  } else {
	 this.MicBalance = false;
  };
  if ( type == "2ch" ) {
	this.Mic2ch = true;
  } else {
	 this.Mic2ch = false;
  };
  if ( type == "maxvol" ) {
	this.MicMax = true;
  } else {
	 this.MicMax = false;
  };
};

WAStim.prototype.changeTypeBalance = function(type) {
 
  if ( type == "maxvol" ) {
	this.MicMax = true;
  } else {
	 this.MicMax = false;
  };
};

WAStim.prototype.MasterMaxVol = function(channel) {
  if ( channel == "left" ) {
	this.MasterVolLeft = this.gainNodeLeft.gain.value;
	this.gainNodeLeft.gain.value = 1;
	// document.getElementById("maxVolLeft").style.borderColor = "red";
	// document.getElementById("maxVolLeft").src = "./speaker_green.png";
  } else if ( channel == "right" ) {
	this.MasterVolRight = this.gainNodeRight.gain.value;
	this.gainNodeRight.gain.value = 1;
	//document.getElementById("maxVolRight").style.borderColor = "red";
  };
};

WAStim.prototype.MasterRestoreVol = function(channel) {
 
  if ( channel == "left" ) {
	this.gainNodeLeft.gain.value = this.MasterVolLeft;
	this.MasterVolLeft = 0.01;
	//document.getElementById("maxVolLeft").style.borderColor = "white";
	// document.getElementById("maxVolLeft").src = "./speaker_green.png";
  } else if ( channel == "right" ) {
	this.gainNodeRight.gain.value = this.MasterVolRight;
	this.MasterVolRight = 0.01;
	//document.getElementById("maxVolRight").style.borderColor = "white";
  };
};

WAStim.prototype.changeTypeMaster = function(type) {
 
  if ( type == "maxvol" ) {
	this.MicMax = true;
  } else {
	 this.MicMax = false;
  };
};


// Fades between 0 (all source 1) and 1 (all source 2)
WAStim.prototype.balance = function(element) {
  var x = parseInt(element.value) / parseInt(element.max);
  if (x == 0) {x = 0.01};
  if (x == 1) {x = 0.99};
  var ratio = this.gainNode1.gain.value / this.gainNode2.gain.value;
	
  // Use an equal-power crossfading curve:
  //var gain1 = Math.cos(v1 * x * 0.5*Math.PI);
  //var gain2 = Math.cos((1.0 - x) * 0.5*Math.PI);
  this.gainNodeLeft.gain.value = (1 - x);
  this.gainNodeRight.gain.value = x;
};

WAStim.prototype.changeVolumeMaster = function(element) {
  //var volume = element.value;
  var fraction = parseInt(element.value) / parseInt(element.max);
  console.log( fraction + " " +  parseInt(element.value) + " " +  parseInt(element.max));
  // Let's use an x*x curve (x-squared) since simple linear (x) does not
  // sound as good.
  this.gainNodeMaster.gain.value = fraction;
};

WAStim.prototype.EnableMaster1 = function(e) {
	if (e === "toggle") {
		this.isPlayingMaster1 = !this.isPlayingMaster1;
	} else if (e === "true") {
		this.isPlayingMaster1 = true;
	} else if (e === "false"){
		this.isPlayingMaster1 = false;
	}
	if (this.isPlayingMaster1) {
		this.muteMaster1.gain.value = 1;
		document.getElementById("MasterMute1").style.borderColor = "blue";
		document.getElementById("MasterMute1").src = "./speaker_green.png";
	} else {
		this.muteMaster1.gain.value = 0.001;
		document.getElementById("MasterMute1").style.borderColor = "red";
		document.getElementById("MasterMute1").src = "./speaker_red.png";
	}
};

WAStim.prototype.EnableMaster2 = function(e) {
	if (e === "toggle") {
		this.isPlayingMaster2 = !this.isPlayingMaster2;
	} else if (e === "true") {
		this.isPlayingMaster2 = true;
	} else if (e === "false"){
		this.isPlayingMaster2 = false;
	}
	if (this.isPlayingMaster2) {
		this.muteMaster2.gain.value = 1;
		document.getElementById("MasterMute2").style.borderColor = "blue";
		document.getElementById("MasterMute2").src = "./speaker_green.png";
	} else {
		this.muteMaster2.gain.value = 0.001;
		document.getElementById("MasterMute2").style.borderColor = "red";
		document.getElementById("MasterMute2").src = "./speaker_red.png";
	}
};
