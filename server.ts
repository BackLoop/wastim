var express = require('express')
var app = express()
var conf = require('./config.json')
var fs = require('fs')
if (conf.proto == "https") {
  // https stuff
  var options = {
    key: fs.readFileSync('./cert/key.txt'),
    cert: fs.readFileSync('./cert/crt.txt'),
    ca: fs.readFileSync('./cert/chain.pem')
  }
  var server = require(conf.proto).createServer(options, app)
  console.log("started in https mode, options: " + options)
}
else {
  var server = require(conf.proto).createServer(app)
  console.log("started in http mode")
}

var io = require('socket.io').listen(server)
var sliders = require('./sliders.json')
var switches = require('./switches.json')

// make the server listen to port x as defined in config.json
server.listen(conf.port)

// serve static pages
app.use(express.static(__dirname + '/public'))
// if the path "/" is requested
app.get('/', function (req, res) {
  // show public/index.html
  res.sendfile(__dirname + '/public/index.html')
})

// Websocket
io.sockets.on('connection', function (socket) {
  // the client is connected
  socket.emit('chat', { mtime: new Date(), text: 'Welcome to WAStim!' })

  // send initial settings
  // socket.emit('settings', sliders)
  // for (slider in sliders) {
  //   io.sockets.emit('slider', { name: slider, value: sliders[slider].Val })
  //   console.log('slider: ' + slider + " " + sliders[slider].Val)
  // }

  // when a user sends a chat text
  socket.on('chat', function (data) {
    // this is sent to all users
    io.sockets.emit('chat', { mtime: new Date(), name: data.name || 'Anonymous', text: data.text })
  })

  // slider movement event received
  socket.on('slider', function (data) {
    // store slider value and transmit to all clients
    sliders[data.name].Val = data.value
    io.sockets.emit('slider', { name: data.name, value: data.value })
  })

  // switch change event received
  socket.on('switch', function (data) {
    // store slider value and transmit to all clients
    console.log(data.name + " old value: " + switches[data.name].Val + " = " + data.value)
    switches[data.name].Val = data.value
    var SwitchID
    for (SwitchID in switches) {
      console.log('settings -> SwitchID : ' + SwitchID + " "
        + switches[SwitchID].Val + " "
        + switches[SwitchID].Action)
      // setSwitch(SwitchID, switches[SwitchID].Val)
    }
    io.sockets.emit('switch', { name: data.name, value: data.value })
  })

  // value change event received
  socket.on('setvalue', function (data) {
    // store slider value and transmit to all clients
    // global[data.name + "Val"] = data.value
    io.sockets.emit('setvalue', { name: data.name, value: data.value })
  })

  socket.on('settings', function (data) {
    // request to send settings
    //                for (i in socket.id) {
    console.log(socket.id)
    //                }
    // data can later be used to retrieve different setting sets
    console.log('sending settings: ' + data.name)
    socket.emit('settings', { sliders: sliders, switches: switches })
  })

})

// write port settings to console
console.log('server is running at http://127.0.0.1:' + conf.port + '/')
