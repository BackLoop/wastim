This is a stereo multi waveform sound generator.
I have been told it can also be used as an estim device, if you have a device that turns the stereo sound into the right voltage...

The server's basic structure is based upon https://github.com/nodecode/Node.js-Chat.

I learned a lot (and used examples) about web audio from Boris Smus at http://webaudioapi.com/
 
The graphml diagrams have been created using [yed](https://www.yworks.com/products/yed)

The doc/wastim.html is a TiddlyWiki that describes some of the internals of this project.

As of end 2017, this project is maintained using [visual studio code](https://code.visualstudio.com/), I am working on converting it into typescript.

This is all still work in progress and in no way meant to be used in any production environment!

Since parts of this project may have been published under different copyright clauses I can not really provide a full working copyright.
The parts that I have written may be used under the BSD 3 clause license