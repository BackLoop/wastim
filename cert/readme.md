Since modern viewers refuse to allow webaudio microphone usage on insecure connections
you have to put your https certs into this directory to enable https serving
(unless you just test run the server and client on your local machine)

you can obtain certs e.g. from letsencrypt.org by using certbot-auto
wget https://dl.eff.org/certbot-auto
chmod +x certbot-auto
./certbot-auto --help
./certbot-auto --apache

then set proto in ../config.json to "https"